﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace DataAccess
{
	// class-context for work with DB
	public class BeautyCenterContext : DbContext
	{
		public BeautyCenterContext()
		{
		}

		public BeautyCenterContext(DbContextOptions<BeautyCenterContext> options)
			: base(options)
		{
			// Automatically creating DB from our context
			// Database.EnsureCreated();
		}

		/// <summary>
		/// Set of entities for users, which are stored in DB (that helps to set up binding between the db set and table in database)
		/// </summary>
		public virtual DbSet<User> Users { get; set; }

		/// <summary>
		/// Set of entities for BeautyServices, which are stored in DB (that helps to set up binding between the db set and table in database)
		/// </summary>
		public virtual DbSet<BeautyService> BeautyServices { get; set; }
		// Set of entities for BeautyServices, which stored in DB(that helps to set up binding between the db set and table in database)
		public virtual DbSet<ServiceMaster> ServiceMasters { get; set; }

		/// <summary>
		/// Configure database connection options
		/// </summary>
		/// <param name="optionsBuilder">Instance of DbContextOptionsBuilder</param>
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			// Setup of connection to sql using connection string
			// TODO: move connection string to application settings
			optionsBuilder.UseSqlServer(/*"Data Source=.\\SQLEXPRESS;Initial Catalog=BeautyCenterDB;Database=BeautyCenterDB;Persist Security Info=True;User ID=test_user;Password=window;Max Pool Size=3000;"*/

											"Server=DESKTOP-1NDKIQU;Database=BeautyCenter;Trusted_Connection=True;");
		}

		//implementation of Fluent API(balance between classes+prop and tables+columns)	

		/// <summary>
		/// Helps to define data base schema using collection of entities and  Fluent API (binding between classes+prop and tables+columns)
		/// </summary>
		/// <param name="modelBuilder">Instance of ModelBuilder</param>
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			//EFCore FluentAPI

			// UserEntity
			modelBuilder.Entity<User>(entity =>
			{
				//Id primary key unique
				entity.HasIndex(e => e.Id).IsUnique();
				// Add No Null constraint for FirstName field
				entity.Property(e => e.FirstName).IsRequired();
				// Add No Null constraint for LastName field
				entity.Property(e => e.LastName).IsRequired();
				// 1:1
				entity.HasOne(user => user.ServiceMaster)
					.WithOne(sm => sm.User)
					.HasForeignKey<User>(u => u.Id);
			});

			// ServiceMasterEntity
			modelBuilder.Entity<ServiceMaster>(entity =>
			{
				entity.HasIndex(e => e.Id).IsUnique();
				// 1:*
				entity.HasMany(i => i.BeautyServices)
					.WithOne(i => i.ServiceMaster)
					.HasForeignKey(i => i.Id);
			});

			// BeautyServiceEntity
			modelBuilder.Entity<BeautyService>(entity =>
			{
				// Add UNIQUE constraint for Id field
				entity.HasIndex(e => e.Id).IsUnique();
				// Add No Null constraint for Name field
				entity.Property(e => e.Name).IsRequired();
				// Add No Null constraint for Price field
				entity.Property(e => e.Price).IsRequired();
			});
		}	
	}
}
