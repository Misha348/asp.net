﻿using System.Collections.Generic;


namespace DataAccess.Entities
{
	public class BaseEntity 
	{
		public int Id { get; set; }
	}

	public class User : BaseEntity
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }

		public int? ServiceMasterId { get; set; }
		public virtual ServiceMaster ServiceMaster { get; set; }
	}

	public class ServiceMaster : BaseEntity
	{
		public int UserId { get; set; }
		public virtual User User { get; set; }
		public virtual ICollection<BeautyService> BeautyServices { get; set; }
	}

	public class BeautyService : BaseEntity
	{
		public string Name { get; set; }
		public double Price { get; set; }

		public int ServiceMasterId { get; set; }
		public virtual ServiceMaster ServiceMaster { get; set; }
	}
}
