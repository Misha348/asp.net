﻿namespace BusinessLogic.Models
{
	// models
	public class BeautyServiceModel
	{
		public string Name { get; set; }
		public double Price { get; set; }
	}
}