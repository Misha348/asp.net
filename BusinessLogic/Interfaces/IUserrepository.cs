﻿using System.Collections.Generic;
using DataAccess.Entities;


namespace BusinessLogic.Interfaces
{
	public interface IUserrepository
	{
		List<User> GetAllUsers();
		User GetUserById(int id);
	}


}
