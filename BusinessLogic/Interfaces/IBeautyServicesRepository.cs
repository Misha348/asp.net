﻿using System.Collections.Generic;
using BusinessLogic.Models;
using DataAccess.Entities;

namespace BusinessLogic.Interfaces
{
	public interface IBeautyServicesRepository
	{
		// return all services
		List<BeautyService> GetAllServices();
		// return services by id
		BeautyService GetServiceById(int id);
	}
}
