﻿using System.Collections.Generic;
using System.Linq;
using BusinessLogic.Interfaces;
using DataAccess;
using DataAccess.Entities;

namespace BusinessLogic.Services
{
	public class BeautyServicesRepository : IBeautyServicesRepository
	{
		private readonly BeautyCenterContext _context;

		public BeautyServicesRepository(BeautyCenterContext context)
		{
			_context = context;
		}

		// getting model by id (implementation of IBeautyServicesRepository)
		public BeautyService GetServiceById(int id)
		{
			return _context.BeautyServices.FirstOrDefault(i => i.Id == id);
		}

		// getting all models (implementation of IBeautyServicesRepository)
		public List<BeautyService> GetAllServices()
		{
			// treatment through context to DB. Rturn services
			return _context.BeautyServices.ToList();
		}
	}
}