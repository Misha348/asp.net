﻿using System.Collections.Generic;
using System.Linq;
using BusinessLogic.Interfaces;
using DataAccess;
using DataAccess.Entities;

namespace BusinessLogic.Services
{
	public class UserRepository : IUserrepository
	{
		private readonly BeautyCenterContext _context;
		public UserRepository(BeautyCenterContext context)
		{
			_context = context;
		}
		public List<User> GetAllUsers()
		{
			return _context.Users.ToList();
		}
		public User GetUserById(int id)
		{
			return _context.Users.FirstOrDefault(i => i.Id == id);
		}
	}
}


