using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace BeautyCenter
{
	// Customn middleware to handle errors. (Should be registered as first middleware)
	public class ErrorHandlingMiddleware
	{
		// Link to next RequestDelegate
		private readonly RequestDelegate _next;

		public ErrorHandlingMiddleware(RequestDelegate next)
		{
			_next = next;
		}

		// All asynchrone methods returns obj Task/Task<T>. " InvokeAsync " - name for all middlewars methods 
		public async Task InvokeAsync(HttpContext context)
		{
			try
			{
				await _next(context);
			}
			catch (Exception e)
			{
				// Handle exception throwed inside of following middlewares
				await context.Response.WriteAsync($"An error occured! {e.Message}");
			}
		}
	}
}