using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace BeautyCenter
{
	public class Program
	{
		// Start of the program
		public static void Main(string[] args)
		{
			// Build and run the application
			CreateHostBuilder(args).Build().Run();
		}

		public static IHostBuilder CreateHostBuilder(string[] args) =>
			Host.CreateDefaultBuilder(args)
				// Configure path that contains all content
				.UseContentRoot(Directory.GetCurrentDirectory())
				// Adds configuration for web host
				.ConfigureWebHostDefaults(webBuilder =>
				{
					webBuilder.UseIISIntegration();
					webBuilder.UseStartup<Startup>();
				});
	}
}
