docker stop $(docker ps -aq)
docker rm $(docker ps -a -q)
docker rmi $(docker images -q) -f
docker build -t jail-cloud .
docker run -p 127.0.0.1:5400:8080/tcp jail-cloud:latest