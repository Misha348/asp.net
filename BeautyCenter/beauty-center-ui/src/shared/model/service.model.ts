import { BaseModel } from './base.model';

export class ServiceModel extends BaseModel {
    name: string;
    price: number;
    serviceMasterId: number
}