export abstract class BaseModel {
    constructor(public id: number) {
    }

    public get _id(): number {
        return this.id;
    }

    public set _id(value: number) {
        this.id = value;
    }
}
