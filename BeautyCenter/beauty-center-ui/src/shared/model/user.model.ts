import { BaseModel } from "./base.model";

export class UserModel extends BaseModel {
    constructor(firstName: string, lastName: string, serviceMasterId: number) {
        super(1);
        this.firstName = firstName;
        this.lastName = lastName;
        this.serviceMasterId = serviceMasterId;
    }
    public firstName: string;
    public lastName: string;
    public serviceMasterId: number;
}