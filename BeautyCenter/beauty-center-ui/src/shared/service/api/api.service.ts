import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { throwError } from 'rxjs';

@Injectable()
export class ApiService {
    constructor(private httpClient: HttpClient) {
    }

    private setHeaders(): HttpHeaders {
        const headersConfig = {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        };
        return new HttpHeaders(headersConfig);
    }

    private formatErrors(error: any): Observable<never> {
        return throwError(error);
    }

    get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
        return this.httpClient.get(`${path}`, { headers: this.setHeaders(), params: params })
            .catch((error: HttpErrorResponse) => this.formatErrors(error));
    }

    put(path: string, body: any = {}): Observable<any> {
        return this.httpClient.put(`${path}`, JSON.stringify(body), { headers: this.setHeaders() })
            .catch((error: HttpErrorResponse) => this.formatErrors(error));
    }

    post(path: string, body: any = {}): Observable<any> {
        return this.httpClient.post(`${path}`, JSON.stringify(body), { headers: this.setHeaders() })
           .catch((error: HttpErrorResponse) => this.formatErrors(error));
    }

    delete(path: string): Observable<any> {
        return this.httpClient.delete(`${path}`, { headers: this.setHeaders() })
           .catch((error: HttpErrorResponse) => this.formatErrors(error));
    }
}