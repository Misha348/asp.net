import { ServiceModel } from 'src/shared/model/service.model';
import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { ApiService } from './api.service';

@Injectable()
export class ServicesService extends BaseService<ServiceModel> {
    constructor(
        protected apiService: ApiService) {
        super(apiService, 'services');
    }
}