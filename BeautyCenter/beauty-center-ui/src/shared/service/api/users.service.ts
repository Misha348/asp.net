import { Injectable, EventEmitter } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BaseService } from './base.service';
import { ApiService } from './api.service';
import { UserModel } from 'src/shared/model/user.model';

@Injectable()
export class UserService extends BaseService<UserModel> {
    constructor(
        protected apiService: ApiService) {
        super(apiService, 'user');
    }
}


