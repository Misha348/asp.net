import { ApiService } from './api.service';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { BaseModel } from 'src/shared/model/base.model';

export abstract class BaseService<TModel extends BaseModel> {
    protected baseUrl: string;

    protected constructor(
        protected apiService: ApiService,
        protected controllerName: string,
    ) {
        this.baseUrl = "http://localhost:5000/api/";
    }

    getSourseLink(): string {
        return this.controllerName;
    }

    getAll(): Promise<TModel[]> {
        return new Promise((resolve, reject) => {
            this.apiService.get(`${this.baseUrl}${this.controllerName}`)
                .subscribe(data => {
                    const result = [];
                    for (let item of data) {
                        result.push(item);
                    }
                    resolve(result);
                }, error => {
                    reject();
                });
        });
    }

    saveRecord(record: TModel): Promise<TModel> {
        return new Promise((resolve, reject) => {
            if (record.id > 0) {
                this.apiService.put(`${this.baseUrl}${this.controllerName}/` + record.id, record)
                    .subscribe(data => {
                        resolve(JSON.parse(data));
                    }, error => {
                        reject();
                    });
            } else {
                this.apiService.post(`${this.baseUrl}${this.controllerName}`, record)
                    .subscribe(data => {
                        resolve(JSON.parse(data));
                    }, error => {
                        reject();
                    });
            }
        });
    }

    getRecordById(id: number): Promise<TModel> {
        return new Promise((resolve, reject) => {
            this.apiService.get(`${this.baseUrl}${this.controllerName}/${id}`)
                .subscribe(data => {
                    resolve(JSON.parse(data));
                }, error => {
                    reject();
                });

        });
    }

    deleteRecordById(id: number): Promise<TModel> {
        return new Promise((resolve, reject) => {
            this.apiService.delete(`${this.baseUrl}${this.controllerName}/${id}`)
                .subscribe(data => {
                    resolve(data);
                }, error => {
                    reject();
                });
        });
    }
}
