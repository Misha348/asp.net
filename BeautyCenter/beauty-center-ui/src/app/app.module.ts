import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ApiService } from '../shared/service/api/api.service';
import { UserService } from 'src/shared/service/api/users.service';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TableModule } from 'primeng/table';
import { UsersListComponent } from './users-list/users-list.component';
import { ServicesListComponent } from './services-list/services-list.component';
import { ServicesService } from 'src/shared/service/api/services.service';

@NgModule({
  declarations: [
    AppComponent,
    UsersListComponent,
    ServicesListComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    TableModule
  ],
  providers: [
    ApiService,
    UserService,
    ServicesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
