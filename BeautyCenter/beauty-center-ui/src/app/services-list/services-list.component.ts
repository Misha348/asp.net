import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/shared/service/api/services.service';
import { ServiceModel } from 'src/shared/model/service.model';

@Component({
  selector: 'app-services-list',
  templateUrl: './services-list.component.html',
  styleUrls: ['./services-list.component.scss']
})
export class ServicesListComponent implements OnInit {
  
  public services: ServiceModel[] = [];

  constructor(private servicesService: ServicesService) { }

  ngOnInit() {
    this.servicesService.getAll().then(data => {
      this.services = data;
    });
  }
}
