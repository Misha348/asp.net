import { Component, OnInit } from '@angular/core';
import { UserModel } from 'src/shared/model/user.model';
import { UserService } from 'src/shared/service/api/users.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  public users: UserModel[] = [];
  
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getAll().then(data => {
      this.users = data;
    });
  }
}
