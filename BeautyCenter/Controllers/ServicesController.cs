﻿using BusinessLogic.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace BeautyCenter.Controllers
{	
	// Attribute to add routing information
	[Route("api/{controller}")]
	public class ServicesController : Controller
	{
		// Interface to inject data repository
		private readonly IBeautyServicesRepository _beautyServicesRepository;

		// during creating this obj(ServicesController) its giving IBeautyServicesRepository\
		// during creating this obj(ServicesController) here will be given real implementation of
		// this service(beautyServicesRepository)
		public ServicesController(IBeautyServicesRepository beautyServicesRepository)
		{
			_beautyServicesRepository = beautyServicesRepository;
		}
		
		// Attribute of http's method Get(require representation of specified resource)
		[HttpGet]
		public ActionResult GetAllServices()
		{
			// Retrieve collection of models from data repository
			var models = _beautyServicesRepository.GetAllServices();
			// Conver list of models to json format
			return new JsonResult(models);
		}

		[HttpGet("{id}")]
		//ActionResult - Universal class for different types(formats ) of returnings(xml, string, json)
		public ActionResult GetServiceById(int id)
		{
			// Retrieve model from data repo by id
			var model = _beautyServicesRepository.GetServiceById(id);
			// Convert single model to json format
			return new JsonResult(model);
		}
	}
}
