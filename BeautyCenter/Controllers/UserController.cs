﻿using BusinessLogic.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace BeautyCenter.Controllers
{
	[Route("api/{controller}")]
	public class UserController : Controller
	{
		private readonly IUserrepository _userRepo;

		public UserController(IUserrepository userRepo)
		{
			_userRepo = userRepo;
		}

		[HttpGet]
		public ActionResult GetAllUsers()
		{
			var models = _userRepo.GetAllUsers();
			return new JsonResult(models);
		}

		[HttpGet("{id}")]
		public ActionResult GetUserById(int id)
		{
			var model = _userRepo.GetUserById(id);
			return new JsonResult(model);
		}
	}
}
