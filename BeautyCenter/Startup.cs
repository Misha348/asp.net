using BusinessLogic.Interfaces;
using BusinessLogic.Services;
using DataAccess;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace BeautyCenter
{
	public class Startup
	{
		private readonly IWebHostEnvironment _env;
		public Startup(IWebHostEnvironment env)
		{
			_env = env;
		}

		// Registration of services
		public void ConfigureServices(IServiceCollection services)
		{
			
			// Register all services and dependent

			// service for registration of all controllers (folder Controller)
			services.AddControllers();

			// Enable Cross Origin resource sharing with options
			services.AddCors(options =>
			{
				options.AddDefaultPolicy(
					builder =>
					{
						builder.WithOrigins("http://localhost:4200");
					});
			});
			// Registration of the custom services:
			// Register custom service by interface to allow dependenct injection. IBeautyServicesRepository -> create such obj(BeautyServicesRepository)
			services.AddTransient<IBeautyServicesRepository, BeautyServicesRepository>();
			// Register custom service by interface to allow dependenct injection. during treatment to IUserRepository -> create such obj(UserRepository)
			services.AddTransient<IUserrepository, UserRepository>();

			ConfigureDatabase(services);
		} 
		 
		// configuration of registration DB
		protected virtual void ConfigureDatabase(IServiceCollection services)
		{
			// registration of context
			services.AddDbContext<BeautyCenterContext>(options =>
			{
				options.UseSqlServer(
					//"Data Source=.\\SQLEXPRESS;Initial Catalog=BeautyCenterDB;Database=BeautyCenterDB;Persist Security Info=True;User ID=test_user;Password=window;Max Pool Size=3000;");
					"Server=DESKTOP-1NDKIQU;Database=BeautyCenter;Trusted_Connection=True;");
				
			},
			// charasteristic of DI(depenndenci injections)
			ServiceLifetime.Transient, ServiceLifetime.Transient);
		}

		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			// Only in development mode (can be changed in launchsettings.json)
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			// Registration of the customn middleware
			app.UseMiddleware(typeof(ErrorHandlingMiddleware));

			// Alows to navigate through wwwrootfolder from browser. I will build all pages automatically
			// app.UseDirectoryBrowser();
			
			// For executing default files (index.html, default.html,  ...)/download root page of programm
			//app.UseDefaultFiles();

			// Allows to access files from static directory (Path can be configured, but by default it is wwwroot )
			// To use static files you can access (localhost:4200/name_of_file)
			// app.UseStaticFiles();


			// during treatment by http server will responce to treat by https
			 app.UseHttpsRedirection();

			// During accessing server by http - server will redirect to https resource
			// app.UseHttpsRedirection();


			// Add middlewares to build routing by controllers automatically. We will use routings
			app.UseRouting();

			// Apply CORS to allow any requests, headers and methods from another origin
			app.UseCors(options => options.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());

			// MapControllers() - It will create mapping between endpoints and method in controllers, 
			// It will add collection of endpoints described inside of controllers.
			// goes to folder Controllers and create inside it diff. useEndpoints,  
			// depending on what  would be written in class Controller, 
			//(rout will get all data from created and registered controller )
			// will set routing in all controllers
			app.UseEndpoints(endpoints => endpoints.MapControllers());

			// Handle custom endpoints
			app.UseEndpoints(endpoints =>
			{
				// Root endpoint (http://localhost:port)
				endpoints.MapGet("/", async context =>
				{
					await context.Response.WriteAsync($"Application Name: {_env.ApplicationName}");
				});
				// Endpoint to check application version (http://localhost:port/version)
				endpoints.MapGet("/version", async context =>
				{
					await context.Response.WriteAsync("v1.0");
				});
			});
		}
	}
}
